<?php

/**
 * @file A description of what is in this content-type.
 */

/**
 * Implementation of hook_node_info().
 */
function advertisement_node_info() {
  $items = array(
    'advertisement' => array(
      'name' => t('Advertisement'),
      'module' => 'features',
      'description' => t('An advertisement'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
