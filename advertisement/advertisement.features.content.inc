<?php

/**
 * @file A basic content type for holding ads
 */

/**
 * Implementation of hook_content_default_fields().
 */
function advertisement_content_default_fields() {
  $fields = array();

  // Exported field: field_probability
  $fields['advertisement-field_probability'] = array(
    'field_name' => 'field_probability',
    'type_name' => 'advertisement',
    'display_settings' => array(
      'weight' => '31',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '1',
    'max' => '100',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '100',
          '_error_element' => 'default_value_widget][field_probability][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Probability',
      'weight' => '31',
      'description' => 'The probability of this ad being showed.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Probability');

  return $fields;
}
